package ru.mintbonda.springtodo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.mintbonda.springtodo.dao.FolderDAO;
import ru.mintbonda.springtodo.dao.TaskDAO;
import ru.mintbonda.springtodo.dao.UserDAO;
import ru.mintbonda.springtodo.models.Folder;
import ru.mintbonda.springtodo.models.Task;
import ru.mintbonda.springtodo.util.UserValidator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
public class UserController {

    private final UserDAO userDAO;

    private final FolderDAO folderDAO;

    private final TaskDAO taskDAO;


    @Autowired
    public UserController(UserDAO userDAO, FolderDAO folderDAO, TaskDAO taskDAO) {
        this.userDAO = userDAO;
        this.folderDAO = folderDAO;
        this.taskDAO = taskDAO;
    }

    @GetMapping("/user/{id}")
    public String userPage(@PathVariable("id") int id, Model model, HttpServletRequest request){

        Cookie[] cookies = request.getCookies();

//        проверка аутентификации пользователя
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user_id") && cookie.getValue().equals(""))
                return "redirect:/home";
        }

        model.addAttribute("user", userDAO.findUser(id));
        model.addAttribute("rootFolder", folderDAO.getUserRootFolder(id));


        return "/user";
    }

    @GetMapping("/user/{id}/folder/{folder_id}")
    public String folderPage(@PathVariable("id") int id, @PathVariable("folder_id") int folder_id,
                             HttpServletRequest request, Model model){

        Cookie[] cookies = request.getCookies();

//        проверка аутентификации пользователя
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user_id") && cookie.getValue().equals(""))
                return "redirect:/home";
        }

        model.addAttribute("user", userDAO.findUser(id));
        model.addAttribute("folders", folderDAO.getUserFolder(id, folder_id));
        model.addAttribute("parentFolder", folderDAO.findFolder(folder_id));
        model.addAttribute("tasks", taskDAO.getUsersTasks(folder_id));

        return "/folders";

    }

    @GetMapping("/user/{id}/folder/{folder_id}/task/{task_id}")
    public String taskPage(@PathVariable("id") int id, @PathVariable("folder_id") int folder_id,
                           @PathVariable("task_id") int task_id, Model model){

        model.addAttribute("task", taskDAO.getUsersTask(task_id));
        model.addAttribute("user_id", id);
        model.addAttribute("folder_id", folder_id);

        return "/task";
    }

    @GetMapping("/user/{id}/folder/{folder_id}/newFolder")
    public String newFolder(@PathVariable("id") int id, @PathVariable("folder_id") int parentFolderId,
                            @ModelAttribute("folder") Folder folder, Model model){

        model.addAttribute("parentFolderId", parentFolderId);
        model.addAttribute("userId", id);

        return "/newFolder";
    }

    @PostMapping("/createFolder")
    public String createFolder(@ModelAttribute("folder") Folder folder,
                               BindingResult bindingResult){

        if (bindingResult.hasErrors())
            return "/error";

        folderDAO.createFolder(folder);

        return "redirect:/user/" + folder.getUserId() + "/folder/" + folder.getParentFolderId();
    }

    @GetMapping("/user/{id}/folder/{folder_id}/newTask")
    public String newTask(@PathVariable("id") int id, @PathVariable("folder_id") int folder_id,
                          @ModelAttribute("task") Task task, Model model){

        model.addAttribute("parentFolderId", folder_id);
        model.addAttribute("userId", id);

        return "/newTask";
    }

    @PostMapping("/createTask/user/{id}")
    public String createTask(@PathVariable("id") int id,
                             @ModelAttribute("task") Task task, BindingResult bindingResult){

        if (bindingResult.hasErrors())
            return "/error";

        taskDAO.createTask(task);

        return "redirect:/user/" + id + "/folder/" + task.getParentFolderId();
    }


    @GetMapping("/user/{id}/folder/{folder_id}/edit")
    public String editFolder(@PathVariable("id") int id, @PathVariable("folder_id") int folder_id,
                             Model model){

        model.addAttribute("folder", folderDAO.findFolder(folder_id));

        return "/editFolder";
    }

    @PostMapping("/folder/{folder_id}/edit")
    public String updateFolder(@PathVariable("folder_id") int folder_id, @ModelAttribute("folder") @Valid Folder updatedFolder,
                               BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            return "/error";
        }

        folderDAO.updateFolder(updatedFolder, folder_id);

        return "redirect:/user/" + updatedFolder.getUserId() + "/folder/" + folder_id;
    }

    @GetMapping("/user/{id}/folder/{folder_id}/task/{task_id}/edit")
    public String editTask(@PathVariable("id") int id, @PathVariable("folder_id") int folder_id,
                           @PathVariable("task_id") int task_id, Model model){

        model.addAttribute("task", taskDAO.getUsersTask(task_id));
        model.addAttribute("userId", id);

        return "/editTask";
    }

    @PostMapping("/user/{id}/task/{task_id}/edit")
    public String updateTask(@PathVariable("task_id") int task_id, @PathVariable("id") int id,
                             @ModelAttribute("task") @Valid Task updatedTask, BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            return "/error";
        }

        taskDAO.updateTask(updatedTask, task_id);

        return "redirect:/user/" + id + "/folder/" + updatedTask.getParentFolderId();
    }

    @GetMapping("/user/{id}/folder/{folder_id}/delete/{parent_id}")
    public String deleteFolder(@PathVariable("id") int user_id, @PathVariable("folder_id") int folder_id,
                               @PathVariable("parent_id") int parent_id){

        folderDAO.deleteFolder(folder_id);

        return "redirect:/user/" + user_id + "/folder/" + parent_id;
    }

    @GetMapping("/user/{id}/folder/{folder_id}/task/delete/{task_id}")
    public String deleteTask(@PathVariable("id") int user_id, @PathVariable("folder_id") int folder_id,
                             @PathVariable("task_id") int task_id){

        taskDAO.deleteTask(task_id);

        return "redirect:/user/" + user_id + "/folder/" + folder_id;
    }

}
