package ru.mintbonda.springtodo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.mintbonda.springtodo.dao.FolderDAO;
import ru.mintbonda.springtodo.dao.UserDAO;
import ru.mintbonda.springtodo.models.User;
import ru.mintbonda.springtodo.util.UserValidator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class MainController {
    private final UserDAO userDAO;
    private final FolderDAO folderDAO;
    private final UserValidator userValidator;

    @Autowired
    public MainController(UserDAO userDAO, FolderDAO folderDAO, UserValidator userValidator) {
        this.userDAO = userDAO;
        this.folderDAO = folderDAO;
        this.userValidator = userValidator;
    }

    @GetMapping("/home")
    public String homePage(HttpServletRequest request){

        Cookie[] cookies = request.getCookies();

//        проверка аутентификации пользователя
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user_id") && !cookie.getValue().equals("")){
                return "redirect:/user/" + cookie.getValue();
            }
        }
        return "/home";
    }

    @GetMapping("/registration")
    public String registrationPage(@ModelAttribute("user") User user){
        return "/registration";
    }

    @PostMapping("/createUser")
    public String createUser(@ModelAttribute("user") @Valid User user,
                             BindingResult bindingResult, HttpServletResponse response){

        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors())
            return "/registration";

        userDAO.createUser(user);

        User authenticatedUser = userDAO.authenticate(user);

        if (authenticatedUser == null)
            return "error";

        folderDAO.createRootFolder(authenticatedUser);

        Cookie userCookie = new Cookie("user_id", String.valueOf(authenticatedUser.getId()));

        response.addCookie(userCookie);

        return "redirect:/user/" + authenticatedUser.getId();
    }

    @GetMapping("/login")
    public String loginPage(@ModelAttribute("user") User user){
        return "/login";
    }

    @PostMapping("/authenticate")
    public String authenticateUser(@ModelAttribute("user") @Valid User user,
                                   BindingResult bindingResult, HttpServletResponse response){

        if (bindingResult.hasErrors()){
            return "/login";
        }

        User authenticatedUser = userDAO.authenticate(user);

        if (authenticatedUser == null)
            return "error";

        Cookie userCookie = new Cookie("user_id", String.valueOf(authenticatedUser.getId()));

        response.addCookie(userCookie);

        return "redirect:/user/" + authenticatedUser.getId();
    }

    @GetMapping("/exit")
    public String exit(HttpServletResponse response){

        Cookie userCookie = new Cookie("user_id", "");

        response.addCookie(userCookie);

        return "redirect:/home";
    }
 }

