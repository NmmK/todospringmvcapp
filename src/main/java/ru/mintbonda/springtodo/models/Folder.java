package ru.mintbonda.springtodo.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class Folder {
    private int id;

    @NotEmpty(message = "Folder name should not be empty")
    @Size(min = 2, max = 30, message = "Folder name should be between 2 and 30 characters")
    private String name;
    private int userId;
    private int parentFolderId;
    private List<Folder> innerFolders;
    private List<Task> tasks;

    public Folder(String name, int userId, int parentFolderId) {
        this.name = name;
        this.userId = userId;
        this.parentFolderId = parentFolderId;
//        this.tasks = new ArrayList<>();
    }

    public Folder() {
    }



    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentFolderId(int parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setInnerFolders(List<Folder> innerFolders) {
        this.innerFolders = innerFolders;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getUserId() {
        return userId;
    }

    public int getParentFolderId() {
        return parentFolderId;
    }

    public List<Folder> getInnerFolders() {
        return innerFolders;
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
