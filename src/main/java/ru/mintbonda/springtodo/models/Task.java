package ru.mintbonda.springtodo.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

public class Task {
    private int id;

    @NotEmpty(message = "Folder name should not be empty")
    @Size(min = 2, max = 30, message = "Folder name should be between 2 and 30 characters")
    private String name;

    private String text;
    private Boolean isCompleted;
    private Timestamp dateOfCreation;
    private Timestamp dateOfUpdate;
    private int parentFolderId;

    public Task(String name, String text, boolean isCompleted, Timestamp dateOfUpdate, int parentFolderId) {
        this.name = name;
        this.text = text;
        this.isCompleted = isCompleted;
        this.dateOfCreation = new Timestamp(System.currentTimeMillis());
        this.dateOfUpdate = new Timestamp(System.currentTimeMillis());
        this.parentFolderId = parentFolderId;
    }

    public Task() {
        this.dateOfCreation = new Timestamp(System.currentTimeMillis());
        this.dateOfUpdate = new Timestamp(System.currentTimeMillis());
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }

    public void setDateOfUpdate(Timestamp dateOfUpdate) {
        this.dateOfUpdate = dateOfUpdate;
    }

    public void setDateOfCreation(Timestamp dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public void setParentFolderId(int parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public Timestamp getDateOfCreation() {
        return dateOfCreation;
    }

    public Timestamp getDateOfUpdate() {
        return dateOfUpdate;
    }

    public int getParentFolderId() {
        return parentFolderId;
    }
}
