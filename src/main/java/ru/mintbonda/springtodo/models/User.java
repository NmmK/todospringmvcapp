package ru.mintbonda.springtodo.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class User {
    private int id;
    @NotEmpty(message = "Login should not be empty")
    @Size(min = 2, max = 30, message = "Login should be between 2 and 30 characters")
    private String login;
    @NotEmpty(message = "Password should not be empty")
    @Size(min = 4, max = 30, message = "Password should be between 4 and 30 characters")
    private String password;

//    private List<Folder> folders;


    public User(int id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
//        добавляется папка по умолчанию - root
//        Folder folder = new Folder("root", id, 0);
    }

    public User() {
    }



    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

}
