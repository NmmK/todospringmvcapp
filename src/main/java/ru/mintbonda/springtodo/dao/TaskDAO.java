package ru.mintbonda.springtodo.dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.mintbonda.springtodo.models.Task;

import java.util.List;

@Component
public class TaskDAO {

    public static String selectUsersTasksQuery = "SELECT * FROM tasks WHERE folder_id=?";

    public static String selectUsersTaskQuery = "SELECT * FROM tasks WHERE task_id=?";

    public static String createUserTaskQuery = "INSERT INTO tasks (task_name, task_text, creation_date, updation_date," +
            "complete, folder_id) VALUES (?, ?, ?, ?, ?, ?)";

    public static String updateTaskQuery = "UPDATE tasks SET task_name=?, task_text=?, updation_date=?, complete=? WHERE task_id=?";

    public static String deleteTaskQuery = "DELETE FROM tasks WHERE task_id=?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TaskDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void createTask(Task task){
        jdbcTemplate.update(createUserTaskQuery, task.getName(), task.getText(), task.getDateOfCreation(), task.getDateOfUpdate(),
                task.getCompleted(), task.getParentFolderId());
    }

    public List<Task> getUsersTasks(int parentFolderID){
        return jdbcTemplate.query(selectUsersTasksQuery, new Object[] {parentFolderID}, new TaskMapper());
    }

    public Task getUsersTask(int task_id){
        return jdbcTemplate.queryForObject(selectUsersTaskQuery, new Object[] {task_id}, new TaskMapper());
    }

    public void updateTask(Task updatedTask, int task_id){
        jdbcTemplate.update(updateTaskQuery, updatedTask.getName(), updatedTask.getText(),
                updatedTask.getDateOfUpdate(), updatedTask.getCompleted(), task_id);
    }

    public void deleteTask(int task_id){
        jdbcTemplate.update(deleteTaskQuery, task_id);
    }

}
