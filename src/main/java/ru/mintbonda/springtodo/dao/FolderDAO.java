package ru.mintbonda.springtodo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.mintbonda.springtodo.models.Folder;
import ru.mintbonda.springtodo.models.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import java.util.List;

@Component
public class FolderDAO {

    public static String selectFolderQuery = "SELECT * FROM folders WHERE folder_id = ?";

    public static String selectUsersRootFoldersQuery = "SELECT * FROM folders WHERE user_id=? AND parent_folder_id is null ";

    public static String createUsersRootFolderQuery = "INSERT INTO folders (folder_name, user_id) VALUES (?, ?)";

    public static String createUsersFolderQuery = "INSERT INTO folders (folder_name, user_id, parent_folder_id) VALUES (?, ?, ?)";

    public static String selectUsersFoldersQuery = "SELECT * FROM folders WHERE user_id=? AND parent_folder_id=?";

    public static String updateFolderQuery = "UPDATE folders SET folder_name=? WHERE folder_id=?";

    public static String deleteFolderQuery = "DELETE FROM folders WHERE folder_id=?";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public FolderDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void createRootFolder(User user){
        jdbcTemplate.update(createUsersRootFolderQuery, user.getLogin() + "_root", user.getId());
    }

    public void createFolder(Folder folder){
        jdbcTemplate.update(createUsersFolderQuery, folder.getName(), folder.getUserId(), folder.getParentFolderId());
    }

    public Folder getUserRootFolder(int id){
        return jdbcTemplate.queryForObject(selectUsersRootFoldersQuery, new Object[] {id}, new FolderMapper());
    }

    public Folder findFolder(int folder_id){
        return jdbcTemplate.queryForObject(selectFolderQuery, new Object[] {folder_id}, new FolderMapper());
    }

    public List<Folder> getUserFolder(int id, int parentFolderID){
        return jdbcTemplate.query(selectUsersFoldersQuery, new Object[] {id, parentFolderID}, new FolderMapper());
    }

    public void updateFolder(Folder updatedFolder, int folder_id){
        jdbcTemplate.update(updateFolderQuery, updatedFolder.getName(), folder_id);
    }

    public void deleteFolder(int folder_id){
        jdbcTemplate.update(deleteFolderQuery, folder_id);
    }

}
