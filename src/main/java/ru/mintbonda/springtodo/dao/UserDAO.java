package ru.mintbonda.springtodo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.mintbonda.springtodo.models.Folder;
import ru.mintbonda.springtodo.models.Task;
import ru.mintbonda.springtodo.models.User;

import java.util.List;
import java.util.Optional;

@Component
public class UserDAO {

    private final JdbcTemplate jdbcTemplate;

    public static String authenticationQuery = "SELECT * FROM users WHERE login=? AND password=?";

    public static String selectWhereQuery = "SELECT * FROM users WHERE id=?";
    public static String selectWhereLoginQuery = "SELECT * FROM users WHERE login=?";
    public static String createUserQuery = "INSERT INTO users (login, password) VALUES (?, ?)";

    @Autowired
    public UserDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public User authenticate(User user) {
        return jdbcTemplate.query(authenticationQuery, new Object[] {user.getLogin(), user.getPassword()},
                        new BeanPropertyRowMapper<>(User.class)).stream().findAny().orElse(null);
    }

    public User findUser(int id){
        return jdbcTemplate.query(selectWhereQuery, new Object[] {id},
                new BeanPropertyRowMapper<>(User.class)).stream().findAny().orElse(null);
    }

    public Optional<User> findUser(String login){
        return jdbcTemplate.query(selectWhereLoginQuery, new Object[] {login},
                new BeanPropertyRowMapper<>(User.class)).stream().findAny();
    }

    public void createUser(User user){
        jdbcTemplate.update(createUserQuery, user.getLogin(), user.getPassword());
    }

}
