package ru.mintbonda.springtodo.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.mintbonda.springtodo.models.Folder;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FolderMapper implements RowMapper<Folder> {
    @Override
    public Folder mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Folder folder = new Folder();

        folder.setId(resultSet.getInt("folder_id"));
        folder.setName(resultSet.getString("folder_name"));
        folder.setUserId(resultSet.getInt("user_id"));

        int parent_folder_id = resultSet.getInt("parent_folder_id");

        if (resultSet.wasNull()) {
            folder.setParentFolderId(0);
        } else {
            folder.setParentFolderId(parent_folder_id);
        }

        return folder;
    }
}
