package ru.mintbonda.springtodo.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.mintbonda.springtodo.models.Task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class TaskMapper implements RowMapper<Task> {
    @Override
    public Task mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Task task = new Task();
        task.setId(resultSet.getInt("task_id"));
        task.setName(resultSet.getString("task_name"));
        task.setText(resultSet.getString("task_text"));
        task.setDateOfCreation(resultSet.getTimestamp("creation_date"));
        Timestamp dateOfUpdate = resultSet.getTimestamp("updation_date");

        if (resultSet.wasNull()) {
            task.setDateOfUpdate(resultSet.getTimestamp("creation_date"));
        } else {
            task.setDateOfUpdate(dateOfUpdate);
        }

        task.setCompleted(resultSet.getBoolean("complete"));
        task.setParentFolderId(resultSet.getInt("folder_id"));


        return task;
    }
}
