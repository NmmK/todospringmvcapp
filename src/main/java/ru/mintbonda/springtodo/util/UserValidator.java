package ru.mintbonda.springtodo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.mintbonda.springtodo.dao.UserDAO;
import ru.mintbonda.springtodo.models.User;

@Component
public class UserValidator implements Validator {
    private final UserDAO userDAO;

    @Autowired
    public UserValidator(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        // посмотреть, есть пользователь с таким же логином
        User newUser = (User) target;

        if (userDAO.findUser(newUser.getLogin()).isPresent()) {
            errors.rejectValue("login", "", "This login is already taken.");
        }
    }
}
